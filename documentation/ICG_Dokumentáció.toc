\select@language {magyar} \contentsline {section}{\numberline {1}Bevezető}{1}{section.1}%
\select@language {magyar} \contentsline {section}{\numberline {2}Felhasználói dokumentáció}{2}{section.2}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.1}Célközönség}{2}{subsection.2.1}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.2}Rendszerkövetelmények}{2}{subsection.2.2}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.3}Program fordítása}{2}{subsection.2.3}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.4}Futtatás}{3}{subsection.2.4}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.5}Haskell Foreign Function Interface és kapcsolódó fogalmai}{3}{subsection.2.5}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.5.1}Saját típusok használata az FFI-vel}{6}{subsubsection.2.5.1}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.5.2}Haskell és C\# típusainak megfeleltetése}{9}{subsubsection.2.5.2}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.6}Működés szemléltetése példán keresztül}{10}{subsection.2.6}%
\select@language {magyar} \contentsline {section}{\numberline {3}Fejlesztői dokumentáció}{28}{section.3}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.1}Modulok kapcsolata}{28}{subsection.3.1}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {3.1.1}KnownTypes}{29}{subsubsection.3.1.1}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {3.1.2}CHeaderParser}{29}{subsubsection.3.1.2}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {3.1.3}CSTypes}{34}{subsubsection.3.1.3}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {3.1.4}CSASTPrinter}{39}{subsubsection.3.1.4}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {3.1.5}Main}{41}{subsubsection.3.1.5}%
\select@language {magyar} \contentsline {section}{\numberline {4}Tesztelés}{42}{section.4}%
\select@language {magyar} \contentsline {subsection}{\numberline {4.1}Rendszerváltozások}{42}{subsection.4.1}%
\select@language {magyar} \contentsline {subsection}{\numberline {4.2}Tesztek}{42}{subsection.4.2}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.2.1}CSASTPrinterTest}{42}{subsubsection.4.2.1}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.2.2}CHeaderParserTest}{42}{subsubsection.4.2.2}%
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.2.3}CSTypesTest}{42}{subsubsection.4.2.3}%

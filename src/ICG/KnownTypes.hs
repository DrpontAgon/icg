module ICG.KnownTypes where

data HsType =  HsVoid | 
               HsChar | 
               HsInt | HsInt8 | HsInt16 | HsInt32 | HsInt64 |
               HsWord | HsWord8 | HsWord16 | HsWord32 | HsWord64 |
               HsFloat | HsDouble | 
               HsBool | HsPtr | HsStablePtr deriving (Show, Read)
module ICG.CSTypes where

import Data.Typeable
import Data.Maybe
import Data.Char
import Data.List
import Foreign.C.Types
import Text.PrettyPrint
import ICG.KnownTypes
import ICG.CHeaderParser
import System.Info

newIntersperse :: a -> [a] -> [a]
newIntersperse _   []     = []
newIntersperse sep (x:xs) = x : go xs
 where
   go []     = []
   go (y:ys) = sep : y : go ys

intercalateWith :: ([a] -> Bool) -> [a] -> [[a]] -> [a]
intercalateWith _ _ [] = []
intercalateWith f sep (xs:xss) = concat (xs : go xss)
    where
        go [] = []
        go (y:ys)
            | f y = sep : y : go ys
            | otherwise = y : go ys

data CSType = CSChar | CSString | 
              CSByte | CSSByte | 
              CSShort | CSUShort | 
              CSInt | CSUInt | 
              CSLong | CSULong | 
              CSBool | 
              CSFloat | CSDouble | CSDecimal | 
              CSVoid | 
              CSObject | 
              CSIntPtr | 
              CSGeneric CSName CSType |
              CSArray CSType |
              CSCustomType CSName deriving Eq

type CSName = String

instance Show CSType where
    show ICG.CSTypes.CSChar = "char"
    show CSString              = "string"
    show CSByte                = "byte"
    show CSSByte               = "sbyte"
    show CSShort               = "short"
    show CSUShort              = "ushort"
    show CSInt                 = "int"
    show CSUInt                = "uint"
    show CSLong                = "long"
    show CSULong               = "ulong"
    show CSBool                = "bool"
    show CSFloat               = "float"
    show CSDouble              = "double"
    show CSDecimal             = "decimal"
    show CSVoid                = "void"
    show CSObject              = "object"
    show CSIntPtr              = "IntPtr"
    show (CSArray t)           = show t ++ "[]"
    show (CSGeneric n m)       = concat [n, "<", show m, ">"]
    show (CSCustomType n)      = n

architectureError :: String
architectureError = "Architecture not supported yet: " ++ arch

hsTypeToCSType :: HsType -> CSType
hsTypeToCSType HsVoid = CSVoid
hsTypeToCSType HsChar = ICG.CSTypes.CSChar
hsTypeToCSType HsInt
    | arch == "i386" = CSInt
    | arch == "x86_64" = CSLong
    | otherwise = errorWithoutStackTrace architectureError
hsTypeToCSType HsInt8 = CSSByte
hsTypeToCSType HsInt16 = CSShort
hsTypeToCSType HsInt32 = CSInt
hsTypeToCSType HsInt64 = CSLong
hsTypeToCSType HsWord
    | arch == "i386" = CSUInt
    | arch == "x86_64" = CSULong
    | otherwise = errorWithoutStackTrace architectureError
hsTypeToCSType HsWord8 = CSByte
hsTypeToCSType HsWord16 = CSUShort
hsTypeToCSType HsWord32 = CSUInt
hsTypeToCSType HsWord64 = CSULong
hsTypeToCSType HsFloat = CSFloat
hsTypeToCSType HsDouble = CSDouble
hsTypeToCSType HsBool = CSBool
hsTypeToCSType HsPtr = CSIntPtr
hsTypeToCSType HsStablePtr = CSIntPtr

generatePrivateAndPublic :: FilePath -> CExternFunction -> (Method, Method)
generatePrivateAndPublic dll (ExternDecl t n@(x:xs) params) = (MethodImport cstype functionName csParams (Just (DllImport dll Ccall (Just Unicode) (Just n))), MethodDef Public NotStatic cstype (toUpper x : xs) csParams (Block [case cstype of CSVoid -> CallFunction (App (Fun functionName) csParamNames); notVoid -> Return (App (Fun functionName) csParamNames) ]) Nothing)
    where 
        cstype = hsTypeToCSType t
        functionName = "hs_" ++ n
        csParams = map (uncurry (Param . hsTypeToCSType)) params
        csParamNames = map (\(Param _ y) -> Var y) csParams

generateMethods :: FilePath ->  [CExternFunction] -> [Method]
generateMethods dll = concatMap ((\(x,y) -> [x,y]) . generatePrivateAndPublic dll)

data Parameter = Param CSType CSName deriving Eq

instance Show Parameter where
    show (Param t n) = show t ++ (' ':n)

data Exp = 
    Var CSName |
    Fun CSName |
    NewArray CSType Exp |
    NewInstance CSType |
    NewInstanceWithParam CSType Exp |
    App Exp [Exp] deriving Eq

instance Show Exp where
    show (Var n) = n
    show (Fun n) = n
    show (NewArray t e) = "new " ++ show t ++ "[" ++ show e ++ "]"
    show (NewInstance t) = "new " ++ show t ++ "()"
    show (NewInstanceWithParam t e) = "new " ++ show t ++ "(" ++ show e ++ ")"
    show (App (Fun a) n) = concat [a, "(", intercalate ", " $ map show n, ")"]

data CallingConvention = Ccall | Stdcall deriving Eq

instance Show CallingConvention where
    show Ccall = "CallingConvention.Cdecl"
    show Stdcall = "CallingConvention.StdCall"

data CharSet = ANSI | Unicode | Auto deriving Eq

instance Show CharSet where
    show ANSI = "CharSet.Ansi"
    show Unicode = "CharSet.Unicode"
    show Auto = "CharSet.Auto"

type EntryPoint = String
data Flag = DllImport FilePath CallingConvention (Maybe CharSet) (Maybe EntryPoint) deriving Eq

instance Show Flag where
    show (DllImport s c cc e) = concat ["[DllImport(", show s, ", CallingConvention = ", show c, case cc of Just charset -> ", CharSet = " ++ show charset; _ -> "", case e of Just entry -> ", EntryPoint = " ++ show entry; _ -> "", ")]\n"]

type LibName = String
newtype UsingStmt = Using LibName deriving Eq

instance Show UsingStmt where
    show (Using l) = "    using " ++ l ++ ";"

addUsings :: CSNameSpace -> [UsingStmt] -> CSNameSpace
addUsings (NameSpace n u c) s = NameSpace n (u ++ s) c

data CSNameSpace = NameSpace CSName [UsingStmt] [CSClass]

instance Show CSNameSpace where
    show (NameSpace n l c) = concat ["namespace ", n, "\n{\n", intercalate "\n" $ map show l, "\n\n", concatMap show c, "\n}"]


addClasses :: CSNameSpace -> [CSClass] -> CSNameSpace
addClasses (NameSpace a u s) cstmt = NameSpace a u (s ++ cstmt)

data CSClass = Class NotPrivateModifier StaticOrSealed CSName (Maybe CSName {-Ősosztály-}) [ClassLevelVariable] [Property] [Method]

instance Show CSClass where
    show (Class m s n inherit vars props l) = intercalateWith (not.null) " " ["   ", show m, show s, "class", n, case inherit of Just i -> ": " ++ i; _ -> ""] ++ "\n    {" ++ concatMap toString vars ++ concatMap toString props ++ concatMap toString l ++ "\n    }"
        where
            toString y = "\n        " ++ show y

data NotPrivateModifier = Internal' | Public' deriving (Eq, Ord)

instance Show NotPrivateModifier where
    show Public' = "public"
    show Internal' = "internal"

data Modifier = Private | Internal | Public deriving (Eq, Ord)

instance Show Modifier where
    show Public = "public"
    show Private = "private"
    show Internal = "internal"

data Statement = VarDecl CSType CSName | Assignment Exp Exp | CallFunction Exp | Return Exp deriving Eq

instance Show Statement where
    show (VarDecl t n) = concat ["            ", show t, ' ':n, ";"]
    show (Assignment (Var e1) e2) = concat ["            ", e1, " = ", show e2, ";"]
    show (CallFunction (App (Fun e1) e2)) = concat ["            ", e1, "(", intercalate ", " $ map show e2, ");"]
    show (Return a) = concat ["            return ", show a, ";"]
    show _ = '\n':errorWithoutStackTrace "Unknown format. Probably incorrect function application."

newtype StatementBlock = Block [Statement] deriving Eq

addStatements :: StatementBlock -> [Statement] -> StatementBlock
addStatements (Block ss) s = Block (ss ++ s)

instance Show StatementBlock where
    show (Block s) = intercalateWith (not.null) "\n" ("        {" : map show s)++"\n        }"
    
data Method = MethodImport CSType CSName [Parameter] (Maybe Flag) | 
              MethodDef Modifier Static CSType CSName [Parameter] StatementBlock (Maybe Flag) |
              Constructor Modifier CSName [Parameter] StatementBlock | 
              Destructor CSName StatementBlock deriving Eq

instance Show Method where
    show (MethodImport t n p f) = intercalateWith (not.null) " " [case f of Just i -> show i; _ -> "", "      ", show Private, show Static, "extern", show t, n] ++ '(' : intercalateWith (not . null) ", " (map show p) ++ ");\n"
    show (MethodDef m s t n p st f) = concat [intercalateWith (not.null) " " [case f of Just i -> show i; _ -> "" ++ show m, show s, show t, n], '(' : intercalateWith (not . null) ", " (map show p), ")\n", show st]
    show (Constructor m n params block) = concat [show m, ' ':n, '(' : intercalate ", " (map show params), ")\n", show block]
    show (Destructor n block) = concat ['~':n, "()\n", show block]
    
addMethods :: CSClass -> [Method] -> CSClass
addMethods (Class m s n inherit c p l) methods = Class m s n inherit c p (l ++ methods)

addMethodsInNamespaceClass :: CSNameSpace -> String -> [Method] -> CSNameSpace
addMethodsInNamespaceClass (NameSpace a b listClass) csclass methods = NameSpace a b (searchAndAddMethod listClass csclass methods [])
    where
        searchAndAddMethod :: [CSClass] -> String -> [Method] -> [CSClass] -> [CSClass]
        searchAndAddMethod [] _ _ a = a
        searchAndAddMethod (x@(Class _ _ n _ _ _ _):xs) c m a
            | n == c = x `addMethods` m : searchAndAddMethod xs c m a
            | otherwise = x : searchAndAddMethod xs c m a

data Static = Static | NotStatic deriving Eq

instance Show Static where
    show Static = "static"
    show NotStatic = ""

data StaticOrSealed = Static' | Sealed' | NotStaticSealed' deriving Eq

instance Show StaticOrSealed where
    show Static' = "static"
    show Sealed' = "sealed"
    show NotStaticSealed' = ""

data Readonly = Readonly | NotReadonly deriving Eq

instance Show Readonly where
    show Readonly = "readonly"
    show NotReadonly = ""

data ClassLevelVariable = 
    Variable Modifier Static Readonly CSType CSName |
    VariableWithNewInstance Modifier Static Readonly CSType CSName Exp

instance Show ClassLevelVariable where
    show (Variable m s r t n) = intercalateWith (not . null) " " [show m, show s, show r, show t, n] ++ ";"
    show (VariableWithNewInstance m s r t n e@(NewInstance _)) = intercalateWith (not.null) " " [show m, show s, show r, show t, n, "=", show e] ++ ";"
    show (VariableWithNewInstance m s r t n e@(NewInstanceWithParam _ _)) = intercalateWith (not.null) " " [show m, show s, show r, show t, n, "=", show e] ++ ";"

data Property = AutoProperty Modifier Static CSType CSName (Either ((Modifier, Maybe StatementBlock), Maybe (Modifier, Maybe StatementBlock)) (Maybe (Modifier, Maybe StatementBlock), (Modifier, Maybe StatementBlock)))

instance Show Property where
    show (AutoProperty m s t n (Left (a,b))) = intercalateWith (not.null) " " [show m, show s, show t, n, case b of Just setter -> showGetSet m (a, setter); _ -> showGet m a]
        where
            showGet :: Modifier -> (Modifier, Maybe StatementBlock) -> String
            showGet m (modifier, Nothing)
                | modifier < m = intercalateWith (not . null) " " ["{", show modifier, "get; }"]
                | otherwise = "{ get; }"
            showGet m (modifier, Just block) = intercalateWith (not . null) "\n" ["\n        {", "        " ++ (if modifier < m then show modifier ++ " " else []) ++ "get", show block, "        }"]
            
            showGetSet :: Modifier -> ((Modifier, Maybe StatementBlock), (Modifier, Maybe StatementBlock)) -> String
            showGetSet m ((modifier1, block1), (modifier2, block2)) = intercalateWith (not . null) "\n" ["\n        {", "        " ++ (if modifier1 < m then show modifier1 ++ " " else []) ++ "get" ++ case block1 of Just b -> '\n' : show b; _ -> ";", "        " ++ (if modifier2 < m then show modifier2 ++ " " else []) ++ "set" ++ case block2 of Just b -> '\n' : show b; _ -> ";", "        }"]

    show (AutoProperty m s t n (Right (a,b))) = intercalateWith (not.null) " " [show m, show s, show t, n, case a of Just getter -> showGetSet m (getter, b); _ -> showSet m b]
        where
            showSet :: Modifier -> (Modifier, Maybe StatementBlock) -> String
            showSet m (modifier, Nothing)
                | modifier < m = intercalateWith (not . null) " " ["{", show modifier, "set; }"]
                | otherwise = "{ set; }"
            showSet m (modifier, Just block) = intercalateWith (not . null) "\n" ["\n        {", "        " ++ (if modifier < m then show modifier ++ " " else []) ++ "set", show block, "        }"]

            showGetSet :: Modifier -> ((Modifier, Maybe StatementBlock), (Modifier, Maybe StatementBlock)) -> String
            showGetSet m ((modifier1, block1), (modifier2, block2)) = intercalateWith (not . null) "\n" ["\n        {", "        " ++ (if modifier1 < m then show modifier1 ++ " " else []) ++ "get" ++ case block1 of Just b -> '\n' : show b; _ -> ";", "        " ++ (if modifier2 < m then show modifier2 ++ " " else []) ++ "set" ++ case block2 of Just b -> '\n' : show b; _ -> ";", "        }"]
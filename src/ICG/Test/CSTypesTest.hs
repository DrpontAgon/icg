{-# LANGUAGE LambdaCase #-}
module ICG.Test.CSTypesTest where

import Test.HUnit
import ICG.KnownTypes
import ICG.CSTypes
import ICG.CHeaderParser
import System.Info

cppAstToCSAstTest1 = TestCase $ do
    let (m1,m2) = generatePrivateAndPublic "testFile1.dll" (ExternDecl HsPtr "proba" [])
    assertBool "C++ -> C# privát" (case m1 of 
        MethodImport CSIntPtr "hs_proba" [] (Just (DllImport "testFile1.dll" Ccall (Just Unicode) (Just "proba"))) -> True
        _ -> False)
    assertBool "C++ -> C# publikus" (case m2 of 
        MethodDef Public NotStatic CSIntPtr "Proba" [] (Block [Return (App (Fun "hs_proba") [])]) Nothing -> True
        _ -> False)

cppAstToCSAstTest2 = TestList (zipWith ((TestCase .) . assertBool) errorMessages bools)
    where
        errorMessages = ["C++ -> C# add privát", "C++ -> C# add publikus", 
                        "C++ -> C# summa privát", "C++ -> C# summa publikus", 
                        "C++ -> C# prod privát", "C++ -> C# prod publikus", 
                        "C++ -> C# multiply privát", "C++ -> C# multiply publikus", 
                        "C++ -> C# osztok privát", "C++ -> C# osztok publikus",
                        "C++ -> C# osztokHossz privát", "C++ -> C# osztokHossz publikus",
                        "C++ -> C# felbontas privát", "C++ -> C# felbontas publikus", 
                        "C++ -> C# felbontasLen privát", "C++ -> C# felbontasLen publikus", 
                        "C++ -> C# teszt privát", "C++ -> C# teszt publikus"]
        bools = zipWith ($)
            [
                \case 
                MethodImport CSInt "hs_add" [Param CSInt "a1", Param CSInt "a2"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "add"))) -> True
                _ -> False,
                \case 
                MethodDef Public NotStatic CSInt "Add" [Param CSInt "a1", Param CSInt "a2"] (Block [Return (App (Fun "hs_add") [Var "a1", Var "a2"])]) Nothing -> True
                _ -> False,
                \case 
                MethodImport CSInt "hs_summa" [Param CSIntPtr "a1", Param CSInt "a2"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "summa"))) -> arch == "i386"
                MethodImport CSInt "hs_summa" [Param CSIntPtr "a1", Param CSLong "a2"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "summa"))) -> arch == "x86_64"
                _ -> False,
                \case 
                MethodDef Public NotStatic CSInt "Summa" [Param CSIntPtr "a1", Param CSInt "a2"] (Block [Return (App (Fun "hs_summa") [Var "a1", Var "a2"])]) Nothing -> arch == "i386"
                MethodDef Public NotStatic CSInt "Summa" [Param CSIntPtr "a1", Param CSLong "a2"] (Block [Return (App (Fun "hs_summa") [Var "a1", Var "a2"])]) Nothing -> arch == "x86_64"
                _ -> False,
                \case 
                MethodImport CSInt "hs_prod" [Param CSIntPtr "a1", Param CSInt "a2"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "prod"))) -> arch == "i386"
                MethodImport CSInt "hs_prod" [Param CSIntPtr "a1", Param CSLong "a2"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "prod"))) -> arch == "x86_64"
                _ -> False,
                \case 
                MethodDef Public NotStatic CSInt "Prod" [Param CSIntPtr "a1", Param CSInt "a2"] (Block [Return (App (Fun "hs_prod") [Var "a1", Var "a2"])]) Nothing -> arch == "i386"
                MethodDef Public NotStatic CSInt "Prod" [Param CSIntPtr "a1", Param CSLong "a2"] (Block [Return (App (Fun "hs_prod") [Var "a1", Var "a2"])]) Nothing -> arch == "x86_64"
                _ -> False,
                \case 
                MethodImport CSInt "hs_multiply" [Param CSInt "a1", Param CSInt "a2"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "multiply"))) -> True
                _ -> False,
                \case 
                MethodDef Public NotStatic CSInt "Multiply" [Param CSInt "a1", Param CSInt "a2"] (Block [Return (App (Fun "hs_multiply") [Var "a1", Var "a2"])]) Nothing -> True
                _ -> False,
                \case 
                MethodImport CSIntPtr "hs_osztok" [Param CSInt "a1"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "osztok"))) -> True
                _ -> False,
                \case 
                MethodDef Public NotStatic CSIntPtr "Osztok" [Param CSInt "a1"] (Block [Return (App (Fun "hs_osztok") [Var "a1"])]) Nothing -> True
                _ -> False,
                \case 
                MethodImport CSInt "hs_osztokHossz" [Param CSInt "a1"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "osztokHossz"))) -> True
                _ -> False,
                \case 
                MethodDef Public NotStatic CSInt "OsztokHossz" [Param CSInt "a1"] (Block [Return (App (Fun "hs_osztokHossz") [Var "a1"])]) Nothing -> True
                _ -> False,
                \case 
                MethodImport CSIntPtr "hs_felbontas" [Param CSInt "a1"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "felbontas"))) -> True
                _ -> False,
                \case 
                MethodDef Public NotStatic CSIntPtr "Felbontas" [Param CSInt "a1"] (Block [Return (App (Fun "hs_felbontas") [Var "a1"])]) Nothing -> True
                _ -> False,
                \case 
                MethodImport CSInt "hs_felbontasLen" [Param CSInt "a1"] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "felbontasLen"))) -> True
                _ -> False,
                \case 
                MethodDef Public NotStatic CSInt "FelbontasLen" [Param CSInt "a1"] (Block [Return (App (Fun "hs_felbontasLen") [Var "a1"])]) Nothing -> True
                _ -> False,
                \case 
                MethodImport CSInt "hs_teszt" [] (Just (DllImport "testFile2.dll" Ccall (Just Unicode) (Just "teszt"))) -> True
                _ -> False,
                \case 
                MethodDef Public NotStatic CSInt "Teszt" [] (Block [Return (App (Fun "hs_teszt") [])]) Nothing -> True
                _ -> False
            ] (generateMethods "testFile2.dll" [
                ExternDecl HsInt32 "add" [(HsInt32, "a1"), (HsInt32, "a2")],
                ExternDecl HsInt32 "summa" [(HsPtr, "a1"), (HsInt, "a2")],
                ExternDecl HsInt32 "prod" [(HsPtr, "a1"), (HsInt, "a2")],
                ExternDecl HsInt32 "multiply" [(HsInt32, "a1"), (HsInt32, "a2")],
                ExternDecl HsPtr "osztok" [(HsInt32, "a1")],
                ExternDecl HsInt32 "osztokHossz" [(HsInt32, "a1")],
                ExternDecl HsPtr "felbontas" [(HsInt32, "a1")],
                ExternDecl HsInt32 "felbontasLen" [(HsInt32, "a1")],
                ExternDecl HsInt32 "teszt" []
                ])
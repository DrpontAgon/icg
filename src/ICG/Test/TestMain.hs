module ICG.Test.TestMain where

import Test.HUnit
import ICG.Test.CHeaderParserTest
import ICG.Test.CSASTPrinterTest
import ICG.Test.CSTypesTest

runTests = runTestTT $ TestList [
    correctParseTest1,
    correctParseTest2,
    correctParseTest3,
    incorrectParseTest1, 
    incorrectParseTest2,
    generateCodeFileTest1,
    generateCodeFileTest2,
    generateCodeFileTest3,
    cppAstToCSAstTest1,
    cppAstToCSAstTest2
    ]
module ICG.Test.CSASTPrinterTest where

import System.IO
import System.Directory
import ICG.CSASTPrinter
import ICG.CSTypes
import Test.HUnit

generateCodeFileTest1 = TestCase $ do
    generateCodeFile (NameSpace "Teszt" [] [])
    str <- readFile "Teszt.cs"
    assertEqual "teszt content" 
        "namespace Teszt\n\
        \{\n\
        \\n\
        \\n\
        \\n\
        \}" str
    removeFile "Teszt.cs"

generateCodeFileTest2 = TestCase $ do
    generateCodeFile (NameSpace "Teszt2" [] [Class Public' NotStaticSealed' "TesztClass" Nothing [] [] []])
    str <- readFile "TesztClass.cs"
    assertEqual "teszt content" 
        "namespace Teszt2\n\
        \{\n\
        \\n\
        \\n\
        \    public class TesztClass\n\
        \    {\n\
        \    }\n\
        \}" str
    removeFile "TesztClass.cs"

generateCodeFileTest3 = TestCase $ do
    generateCodeFile (NameSpace "Teszt3" (Using <$> ["System", "Alma"]) [Class Public' Sealed' "TesztClass2" (Just "Proba") [] [] [
        MethodDef Public NotStatic CSVoid "TesztMetodus" [] (Block []) Nothing
        ]])
    str <- readFile "TesztClass2.cs"
    assertEqual "teszt content" 
        "namespace Teszt3\n\
        \{\n\
        \    using System;\n\
        \    using Alma;\n\
        \\n\
        \    public sealed class TesztClass2 : Proba\n\
        \    {\n\
        \        public void TesztMetodus()\n\
        \        {\n\
        \        }\n\
        \    }\n\
        \}" str
    removeFile "TesztClass2.cs"
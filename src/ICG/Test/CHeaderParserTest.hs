{-# LANGUAGE LambdaCase #-}
module ICG.Test.CHeaderParserTest where

import Test.HUnit
import ICG.CHeaderParser
import ICG.KnownTypes

testFile1 :: String
testFile1 = "#include \"HsFFI.h\"\n\
            \#ifdef __cplusplus\n\
            \extern \"C\" {\n\
            \#endif\n\
            \extern HsInt32 add(HsInt32 a1, HsInt32 a2);\n\
            \extern HsInt32 summa(HsPtr a1, HsInt a2);\n\
            \extern HsInt32 prod(HsPtr a1, HsInt a2);\n\
            \extern HsInt32 multiply(HsInt32 a1, HsInt32 a2);\n\
            \extern HsPtr osztok(HsInt32 a1);\n\
            \extern HsInt32 osztokHossz(HsInt32 a1);\n\
            \extern HsPtr felbontas(HsInt32 a1);\n\
            \extern HsInt32 felbontasLen(HsInt32 a1);\n\
            \extern HsInt32 teszt(HsInt32 a1);\n\
            \#ifdef __cplusplus\n\
            \}\n\
            \#endif\n"

testFile2 :: String
testFile2 = ""

testFile3 :: String
testFile3 = "#include \"HsFFI.h\"\n\
            \#ifdef __cplusplus\n\
            \extern \"C\" {\n\
            \#endif\n\
            \extern void f(void);\n\
            \extern HsInt32 s(HsPtr a1);\n\
            \extern HsInt32 p(HsInt a1);\n\
            \extern HsBool m(HsInt32 a1);\n\
            \extern HsPtr os(void);\n\
            \extern HsDouble test(HsFloat a2);\n\
            \#ifdef __cplusplus\n\
            \}\n\
            \#endif\n"

testFile4 :: String
testFile4 = "teljesen rossz\n\
            \nincs egy fuggveny sem\n\
            \ettol\n\
            \meg\n\
            \nem fog\n\
            \a\n\
            \parser\n\
            \elhasalni\n"

testFile5 :: String
testFile5 = "#include \"HsFFI.h\"\n\
            \#ifdef __cplusplus\n\
            \extern \"C\" {\n\
            \#endif\n\
            \extern void f(void) // lemaradt a pontosvesszo\n\
            \extern HsInt32 s(HsPtr a1);\n\
            \#ifdef __cplusplus\n\
            \}\n\
            \#endif\n"

parseTestFile :: String -> (Bool, [CExternFunction])
parseTestFile str = case parseData str of Left _ -> (False, []); Right t -> (True, t)

correctParseTest1 = TestList (TestCase (assertBool "C++ Header jó olvasás!" rightParse) : zipWith ((TestCase .) . assertBool) errorMessages bools)
    where
        (rightParse, parsed) = parseTestFile testFile1
        errorMessages = ["C++ header add", "C++ header summa", "C++ header prod", "C++ header multiply", "C++ header osztok", "C++ header osztokHossz",
                        "C++ header felbontas", "C++ header felbontasLen", "C++ header teszt"]
        bools = zipWith ($) [
            \case
                ExternDecl HsInt32 "add" [(HsInt32, "a1"), (HsInt32, "a2")] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "summa" [(HsPtr, "a1"), (HsInt, "a2")] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "prod" [(HsPtr, "a1"), (HsInt, "a2")] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "multiply" [(HsInt32, "a1"), (HsInt32, "a2")] -> True
                _ -> False,
            \case
                ExternDecl HsPtr "osztok" [(HsInt32, "a1")] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "osztokHossz" [(HsInt32, "a1")] -> True
                _ -> False,
            \case
                ExternDecl HsPtr "felbontas" [(HsInt32, "a1")] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "felbontasLen" [(HsInt32, "a1")] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "teszt" [(HsInt32, "a1")] -> True
                _ -> False
         ] parsed

correctParseTest2 = TestList [TestCase $ assertBool "C++ header üres!" rightParse, TestCase $ assertBool "Üres helyes olvasás." $ null parsed]
    where
        (rightParse, parsed) = parseTestFile testFile2

correctParseTest3 = TestList (TestCase (assertBool "C++ Header jó olvasás voiddal!" rightParse) : zipWith ((TestCase .) . assertBool) errorMessages bools)
    where
        (rightParse, parsed) = parseTestFile testFile3
        errorMessages = ["C++ header f", "C++ header s", "C++ header p", "C++ header m", "C++ header os", "C++ header test"]
        bools = zipWith ($) [
            \case
                ExternDecl HsVoid "f" [] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "s" [(HsPtr, "a1")] -> True
                _ -> False,
            \case
                ExternDecl HsInt32 "p" [(HsInt, "a1")] -> True
                _ -> False,
            \case
                ExternDecl HsBool "m" [(HsInt32, "a1")] -> True
                _ -> False,
            \case
                ExternDecl HsPtr "os" [] -> True
                _ -> False,
            \case
                ExternDecl HsDouble "test" [(HsFloat, "a2")] -> True
         ] parsed

incorrectParseTest1 = TestList [TestCase $ assertBool "C++ header rossz" wrongParse, TestCase $ assertBool "Nem olvastunk semmit." $ null parsed]
    where
        (wrongParse, parsed) = parseTestFile testFile4

incorrectParseTest2 = TestList [TestCase $ assertBool "C++ header pontosvessző" rightParse, TestCase $ assertBool "Nem olvastunk semmit." $ 
    case parsed of [ExternDecl HsInt32 "s" [(HsPtr, "a1")]] -> True; _ -> False]
    where
        (rightParse, parsed) = parseTestFile testFile5
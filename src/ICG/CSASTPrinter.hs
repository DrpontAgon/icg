module ICG.CSASTPrinter where
    
import ICG.CHeaderParser (getExternFunctionsFromCHeader)
import ICG.CSTypes
import Data.Char (isDigit)
import System.IO

initialCode :: String -> String -> FilePath -> CSNameSpace
initialCode nameSpaceName className dll = NameSpace nameSpaceName (Using <$> ["System", "System.Runtime.InteropServices"]) [
    Class Public' Sealed' className Nothing [createInstanceVariable] [createAutoProperty] [
        createInstanceMethod,
        createConstructor,
        createDestructor,
        createHsInit,
        createHsExit
        ]]
        where
            createInstanceVariable = VariableWithNewInstance Private Static Readonly (CSGeneric "Lazy" (CSCustomType className)) "Lazy" (NewInstanceWithParam (CSGeneric "Lazy" (CSCustomType className)) (Fun "CreateInstance"))

            createAutoProperty = AutoProperty Public Static (CSCustomType className) "HaskellContext" (Left ((Public, Just $ Block [Return $ Var "Lazy.Value"]), Nothing))

            createInstanceMethod = MethodDef Private Static (CSCustomType className) "CreateInstance" [] (Block [Return $ NewInstance (CSCustomType className)]) Nothing

            createConstructor = Constructor Private className [] (Block [CallFunction (App (Fun "hs_init") [Var "IntPtr.Zero", Var "IntPtr.Zero"])])
            
            createDestructor = Destructor className (Block [CallFunction $ App (Fun "hs_exit") []])

            createHsInit = MethodImport CSVoid "hs_init" [Param CSIntPtr "a1", Param CSIntPtr "a2"] (Just (DllImport dll Ccall Nothing Nothing))

            createHsExit = MethodImport CSVoid "hs_exit" [] (Just (DllImport dll Ccall Nothing Nothing))

generateCode :: String -> String -> FilePath -> FilePath -> IO (Either String CSNameSpace)
generateCode [] _ _ _ = return $ Left "Névtérnév nem lehet üres!"
generateCode _ [] _ _ = return $ Left "Osztálynév nem lehet üres!"
generateCode nameSpace@(n:ameSpace) className@(c:lassName) cheader dll = do
    let isEnglishLetter = (`elem` (['A' .. 'Z'] ++ ['a'..'z']))
    if isEnglishLetter n && all (\x -> isEnglishLetter x || isDigit x || x == '.') ameSpace && and (zipWith (\x y -> x /= '.' || (y /= '.' && not (isDigit y))) ameSpace (tail ameSpace)) then
        if isEnglishLetter c && all (\x -> isEnglishLetter x || isDigit x) lassName then
            do
                funcs <- getExternFunctionsFromCHeader cheader
                return $ Right $ addMethodsInNamespaceClass (initialCode nameSpace className dll) className (generateMethods dll funcs)
        else
            return $ Left "Az osztálynév csak az angol ábécé betűiből és számjegyekből állhat.\nAz első karakter csak betű lehet."
    else
        return $ Left "A névtér névben csak az angol ábécé betűi, számok vagy pontok szerepelhetnek.\nAz első karakter csak betű lehet, továbbá nem állhat két pont közvetlen egymás után,\nilletve nem állhat pont után számjegy."

generateCodeFile :: CSNameSpace -> IO ()
generateCodeFile ns@(NameSpace n _ []) = writeFile (n ++ ".cs") $ show ns
generateCodeFile ns@(NameSpace _ _ (Class _ _ n _ _ _ _ : _)) = writeFile (n ++ ".cs") $ show ns
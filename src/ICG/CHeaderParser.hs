module ICG.CHeaderParser where

import Text.ParserCombinators.Parsec
import Text.Parsec hiding ((<|>), try)
import Text.Parsec.Char
import Control.Monad
import Control.Applicative hiding (some, many, (<|>), optional)
import ICG.KnownTypes
import Data.Maybe (isJust, fromJust)

type FunctionName = String
type CParameter = (HsType, String)

data CExternFunction = ExternDecl HsType FunctionName [CParameter]

instance Show CExternFunction where
    show (ExternDecl HsVoid f []) = "extern void " ++ f ++ "(void);"
    show (ExternDecl HsVoid f p ) = "extern void " ++ f ++ '(' : init (concatMap (\(x,y) -> show x ++ ' ':y ++ ",") p) ++ ");"
    show (ExternDecl t f [])      = "extern " ++ show t ++ ' ':f ++ "(void);"
    show (ExternDecl t f p )      = "extern " ++ show t ++ ' ':f ++ '(' : init (concatMap (\(x,y) -> show x ++ ' ':y ++ ",") p) ++ ");"

parseVoidOrType :: String -> HsType
parseVoidOrType "void" = HsVoid
parseVoidOrType s      = read s

parseHsType :: Parser HsType
parseHsType = fmap parseVoidOrType (many1 (letter <|> digit)) <* spaces

parseVariableName :: Parser String
parseVariableName = do
    c <- letter <|> char '_'
    cs <- many (letter <|> digit <|> char '_')
    return (c:cs)

parseParameter :: Parser CParameter
parseParameter = do
    hstype <- parseHsType
    spaces
    s <- parseVariableName
    spaces
    return (hstype, s)

parseParameterList :: Parser [CParameter]
parseParameterList = between (char '(' <* spaces) (char ')' <* spaces) $ (string "void" >> return []) <|> sepBy parseParameter (char ',' <* spaces)

parseExtern :: Parser CExternFunction
parseExtern = do
    string "extern"
    spaces
    hstype <- parseHsType
    s <- parseVariableName
    spaces
    parameters <- parseParameterList
    char ';'
    spaces
    return $ ExternDecl hstype s parameters

parseFilter :: (a -> Bool) -> Parser [a] -> Parser [a]
parseFilter p parser = filter p <$> parser

parseMap :: (a -> b) -> Parser [a] -> Parser [b]
parseMap f parser = map f <$> parser

parseCHeader :: Parser [CExternFunction]
parseCHeader = (eof >> return []) <|> 
               parseMap fromJust 
                 (parseFilter isJust (manyTill (manyTill anyChar (void (try (lookAhead parseExtern)) <|> void eof) >> optionMaybe parseExtern) eof))

type CHeaderFilePath = FilePath

parseData :: String -> Either ParseError [CExternFunction]
parseData = parse parseCHeader "C/C++ header file"

getExternFunctionsFromCHeader :: CHeaderFilePath -> IO [CExternFunction]
getExternFunctionsFromCHeader path = do
    dataToParse <- readFile path
    return $ case parseData dataToParse of
        Left _ -> []
        Right t -> t
{-# LANGUAGE ScopedTypeVariables #-}
module NewArray(NewArray, NewArrayView(..), newArrayView, newForeignArray) where

import Foreign
import Data.Int
import Data.List (genericLength)

data NewArray e = NewArray Int32 (Ptr e)
data NewArrayView e = NewArrayV Int32 (Ptr e)

newArrayView :: NewArray e -> NewArrayView e
newArrayView (NewArray i ptr) = NewArrayV i ptr

newForeignArray :: Storable a => [a] -> IO (NewArray a)
newForeignArray l = NewArray (genericLength l) <$> newArray l

instance Storable (NewArray e) where
    sizeOf a = let packedSize = sizeOf (undefined :: Int32) + sizeOf (undefined :: Ptr e); align = alignment a; (x,r) = packedSize `divMod` align in
        (x + fromEnum(r /= 0)) * align
    alignment _ = max (alignment (undefined :: Int32)) (alignment (undefined :: Ptr e))
    poke dest a@(NewArray i ptr) = do
        poke (castPtr dest) i
        poke (dest `plusPtr` alignment a) ptr
    peek ptr = do
        i <- peek (castPtr ptr) :: IO Int32
        l <- peek (ptr `plusPtr` alignment (undefined :: NewArray e))
        return $ NewArray i l
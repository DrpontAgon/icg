{-# LANGUAGE ForeignFunctionInterface #-}
module FFIProba where

import Foreign.C.Types
import Foreign.Marshal.Array
import Foreign.Storable
import NewArray
import Foreign
import Data.Word
import Data.List

-- foreign export és az exportáltfüggvény egy fordítási egysében kell legyen.
foreign export ccall myFree :: Ptr a -> IO ()
myFree = free

-- tiszta függvények, ezek csak egy adott bemenetre csak egyszer futnak le.

foreign export ccall "addInt" add :: CInt -> CInt -> CInt
foreign export ccall "addDouble" add :: CDouble -> CDouble -> CDouble

add :: (Eq a, Num a) => a -> a -> a
add 0 x = x
add x 0 = x
add x y = x + y

-- Ptr tömbök, listák (haskell és c# oldalon)
-- rekurzív splitOn

foreign export ccall "splitOnInt" splitOn :: CInt -> Ptr (NewArray CInt) -> IO (Ptr (NewArray (NewArray CInt)))

splitOn :: (Storable a, Eq a) => a -> Ptr (NewArray a) -> IO (Ptr (NewArray (NewArray a)))
splitOn e ptr = do
    NewArrayV i ls <- newArrayView <$> peek ptr
    hsList <- peekArray (fromIntegral i) ls
    arr <- mapM newForeignArray $ splitOn' e hsList
    newForeignArray arr >>= new

splitOn' :: Eq a => a -> [a] -> [[a]]
splitOn' e [] = [[]]
splitOn' e (x : xs)
    | e == x = [] : splitOn' e xs
    | otherwise = (x : y) : ys
    where
        (y:ys) = splitOn' e xs

foreign export ccall divisors :: CInt -> IO (Ptr (NewArray CInt))

divisors :: (Storable i, Integral i) => i -> IO (Ptr (NewArray i))
divisors num = newForeignArray ([x | x <- [1..num `div` 2], num `mod` x == 0] ++ [num]) >>= new

foreign export ccall listListSum :: Ptr (NewArray (NewArray CLLong)) -> IO CLLong

listListSum :: (Num a, Storable a) => Ptr (NewArray (NewArray a)) -> IO a
listListSum ptr = do
    NewArrayV i ptrs <- newArrayView <$> peek ptr
    ptrList <- peekArray (fromIntegral i) ptrs
    hsListList <- mapM ((\(NewArrayV n p) -> peekArray (fromIntegral n) p) . newArrayView) ptrList
    return $ sum $ map sum hsListList
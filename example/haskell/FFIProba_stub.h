#include "HsFFI.h"
#ifdef __cplusplus
extern "C" {
#endif
extern void myFree(HsPtr a1);
extern HsInt32 addInt(HsInt32 a1, HsInt32 a2);
extern HsDouble addDouble(HsDouble a1, HsDouble a2);
extern HsPtr splitOnInt(HsInt32 a1, HsPtr a2);
extern HsPtr divisors(HsInt32 a1);
extern HsInt64 listListSum(HsPtr a1);
#ifdef __cplusplus
}
#endif


﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace HaskellProbaForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int p1 = 0, p2 = 0;
            if (CoalesceException(() => p1 = TextboxToType<int>(textBox1),
                    () => MessageBox.Show("Nem egész szám az első paraméter.")) &&
                CoalesceException(() => p2 = TextboxToType<int>(textBox2),
                    () => MessageBox.Show("Nem egész szám a második paraméter.")))
            {
                MessageBox.Show(FFIProba.HaskellContext.AddInt(p1, p2).ToString(CultureInfo.InvariantCulture));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double p1 = 0, p2 = 0;
            if (CoalesceException(() => p1 = TextboxToType<double>(textBox3),
                    () => MessageBox.Show("Nem tört szám az első paraméter.")) &&
                CoalesceException(() => p2 = TextboxToType<double>(textBox4),
                    () => MessageBox.Show("Nem tört szám a második paraméter.")))
            {
                MessageBox.Show(FFIProba.HaskellContext.AddDouble(p1, p2).ToString(CultureInfo.InvariantCulture));
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int p1 = 0;
            int[] p2 = null;
            if (!CoalesceException(
                    () => p1 = TextboxToType<int>(textBox10),
                    () => MessageBox.Show("Nem egész szám a paraméter.")) ||
                !CoalesceException(
                    () => p2 = textBox11.Text.Split(',').Select(x => Convert.ToInt32(x)).ToArray(),
                    () => MessageBox.Show("Nem megfelelő tömb formátum vagy valamely elem nem egész szám."))) return;
            StorableArray a = new StorableArray(p2);
            var ptr = Marshal.AllocHGlobal(Marshal.SizeOf<StorableArray>());
            Marshal.StructureToPtr(a, ptr, true);
            var s = FFIProba.HaskellContext.SplitOnInt(p1, ptr);
            StorableArray res = Marshal.PtrToStructure<StorableArray>(s);
            StorableArray[] arrays = new StorableArray[res.Length];
            MarshalExtension.Copy(res.Ptr, arrays, 0, res.Length);
            Marshal.FreeHGlobal(ptr);
            int[][] result = new int[arrays.Length][];
            for (int i = 0; i < arrays.Length; i++)
            {
                result[i] = new int[arrays[i].Length];
                Marshal.Copy(arrays[i].Ptr, result[i], 0, arrays[i].Length);
            }

            a.CsFree();
            foreach (var array in arrays)
            {
                array.HaskellFree();
            }

            res.HaskellFree();
            FFIProba.HaskellContext.MyFree(s);
            MessageBox.Show(ArrayToString(result));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int p1 = 1;
            if (!CoalesceException(
                () => p1 = TextboxToType<int>(textBox12),
                () => MessageBox.Show("Nem egész szám a paraméter.")))
                return;
            var s = FFIProba.HaskellContext.Divisors(p1);
            var a = Marshal.PtrToStructure<StorableArray>(s);
            if (a.Length == 0)
            {
                MessageBox.Show(string.Empty);
                return;
            }

            int[] divisors = new int[a.Length];
            Marshal.Copy(a.Ptr, divisors, 0, a.Length);

            a.HaskellFree();
            FFIProba.HaskellContext.MyFree(s);
            MessageBox.Show(ArrayToString(divisors));
        }

        private void button12_Click(object sender, EventArgs e)
        {
            long[][] p1 = null;
            if (!CoalesceException(
                () => p1 = textBox17.Text.Split(';').Select(x => x.Split(',').Select(y => Convert.ToInt64(y)).ToArray()).ToArray(),
                () => MessageBox.Show("Nem megfelelő a formátum vagy valamely elem nem egész szám."))) return;
            StorableArray[] a = new StorableArray[p1.Length];
            for (int i = 0; i < p1.Length; i++)
            {
                a[i] = new StorableArray(p1[i]);
            }

            StorableArray b = new StorableArray(a);
            var ptr = Marshal.AllocHGlobal(Marshal.SizeOf<StorableArray>());
            Marshal.StructureToPtr(b, ptr, true);
            MessageBox.Show(FFIProba.HaskellContext.ListListSum(ptr).ToString());
            Marshal.FreeHGlobal(ptr);
            foreach (var item in a)
            {
                item.CsFree();
            }

            b.CsFree();
        }

        private static T TextboxToType<T>(TextBox t)
        {
            return (T) Convert.ChangeType(t.Text, typeof(T), CultureInfo.InvariantCulture);
        }

        private static string ArrayToString<T>(T[] a)
        {
            if (typeof(T).IsArray)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("[");
                if (a.Length != 0)
                {
                    sb.Append(ArrayToString((dynamic) a[0]));
                }

                foreach (var item in a.Skip(1))
                {
                    sb.AppendLine(",");
                    sb.Append($"{ArrayToString((dynamic) item)}");
                }

                sb.AppendLine("]");
                return sb.ToString();
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                if (a.Length != 0)
                {
                    sb.Append(a[0]);
                }

                foreach (var item in a.Skip(1))
                {
                    sb.Append($", {item}");
                }

                sb.AppendLine("]");
                return sb.ToString();
            }
        }

        private static bool CoalesceException(Action tryF, Action catchF)
        {
            try
            {
                tryF();
                return true;
            }
            catch
            {
                catchF();
                return false;
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public class StorableArray
    {
        public int Length { get; }
        public IntPtr Ptr { get; }

        public StorableArray()
        {
            Length = 0;
            Ptr = IntPtr.Zero;
        }

        public StorableArray(int[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(short[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(short)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(byte[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(long[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(long)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(char[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(char)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(double[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(double)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(float[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(float)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(IntPtr[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IntPtr)) * a.Length);
            Marshal.Copy(a, 0, Ptr, a.Length);
        }

        public StorableArray(StorableArray[] a)
        {
            Length = a.Length;
            Ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(StorableArray)) * a.Length);
            MarshalExtension.Copy(a, 0, Ptr, a.Length);
        }

        public void CsFree()
        {
            Marshal.FreeHGlobal(Ptr);
        }

        public void HaskellFree()
        {
            FFIProba.HaskellContext.MyFree(Ptr);
        }
    }

    public static class MarshalExtension
    {
        public static void Copy<T>(IntPtr unmanagedArray, T[] managedArray, int index, int length)
        {
            var size = Marshal.SizeOf(typeof(T));
            for (int i = index; i < length; i++)
            {
                IntPtr ins = new IntPtr((Environment.Is64BitOperatingSystem ? unmanagedArray.ToInt64() : unmanagedArray.ToInt32()) + i * size);
                managedArray[i] = Marshal.PtrToStructure<T>(ins);
            }
        }

        public static void Copy<T>(T[] managedArray, int index, IntPtr unmanagedArray, int length)
        {
            var size = Marshal.SizeOf(typeof(T));
            for (int i = index; i < length; i++)
            {
                IntPtr ins = new IntPtr((Environment.Is64BitOperatingSystem ? unmanagedArray.ToInt64() : unmanagedArray.ToInt32()) + i * size);
                Marshal.StructureToPtr(managedArray[i], ins, true);
            }
        }
    }
}
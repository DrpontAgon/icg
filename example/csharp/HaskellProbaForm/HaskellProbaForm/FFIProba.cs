namespace HaskellProbaForm
{
    using System;
    using System.Runtime.InteropServices;

    public sealed class FFIProba
    {
        private static readonly Lazy<FFIProba> Lazy = new Lazy<FFIProba>(CreateInstance);
        public static FFIProba HaskellContext 
        {
        get
        {
            return Lazy.Value;
        }
        }
        private static FFIProba CreateInstance()
        {
            return new FFIProba();
        }
        private FFIProba()
        {
            hs_init(IntPtr.Zero, IntPtr.Zero);
        }
        ~FFIProba()
        {
            hs_exit();
        }
        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void hs_init(IntPtr a1, IntPtr a2);

        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void hs_exit();

        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "myFree")]
        private static extern void hs_myFree(IntPtr a1);

        public void MyFree(IntPtr a1)
        {
            hs_myFree(a1);
        }
        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "addInt")]
        private static extern int hs_addInt(int a1, int a2);

        public int AddInt(int a1, int a2)
        {
            return hs_addInt(a1, a2);
        }
        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "addDouble")]
        private static extern double hs_addDouble(double a1, double a2);

        public double AddDouble(double a1, double a2)
        {
            return hs_addDouble(a1, a2);
        }
        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "splitOnInt")]
        private static extern IntPtr hs_splitOnInt(int a1, IntPtr a2);

        public IntPtr SplitOnInt(int a1, IntPtr a2)
        {
            return hs_splitOnInt(a1, a2);
        }
        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "divisors")]
        private static extern IntPtr hs_divisors(int a1);

        public IntPtr Divisors(int a1)
        {
            return hs_divisors(a1);
        }
        [DllImport("C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\example\\haskell\\FFIProba.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "listListSum")]
        private static extern long hs_listListSum(IntPtr a1);

        public long ListListSum(IntPtr a1)
        {
            return hs_listListSum(a1);
        }
    }
}
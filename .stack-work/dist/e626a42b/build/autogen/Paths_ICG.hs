{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_ICG (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,1] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\.stack-work\\install\\e339e44f\\bin"
libdir     = "C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\.stack-work\\install\\e339e44f\\lib\\x86_64-windows-ghc-8.6.5\\ICG-0.1.0.1-JnyYuAQHEk3613vsueVdQ4"
dynlibdir  = "C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\.stack-work\\install\\e339e44f\\lib\\x86_64-windows-ghc-8.6.5"
datadir    = "C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\.stack-work\\install\\e339e44f\\share\\x86_64-windows-ghc-8.6.5\\ICG-0.1.0.1"
libexecdir = "C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\.stack-work\\install\\e339e44f\\libexec\\x86_64-windows-ghc-8.6.5\\ICG-0.1.0.1"
sysconfdir = "C:\\Users\\Viktor\\Documents\\StackProjects\\ICG\\.stack-work\\install\\e339e44f\\etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "ICG_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "ICG_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "ICG_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "ICG_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "ICG_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "ICG_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "\\" ++ name)

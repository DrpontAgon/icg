{-# LANGUAGE LambdaCase #-}
module Main where

import ICG.CSASTPrinter
import Data.List
import Control.Concurrent (threadDelay)
import Control.Monad
import Control.Monad.IO.Class
import ICG.CHeaderParser (getExternFunctionsFromCHeader, CExternFunction(..))
import ICG.Test.TestMain
import System.IO
import System.Directory (doesFileExist, canonicalizePath)
import System.Environment (getArgs)

putStr' :: String -> IO ()
putStr' str = do
    putStr str
    hFlush stdout

whileM_ :: Monad m => m a -> m Bool -> m ()
whileM_ action conditon = do
    cond <- conditon
    when cond $ action >> whileM_ action conditon

prompt :: String -> IO String
prompt msg = putStr' msg >> getLine

main :: IO ()
main = do
    args <- getArgs
    case args of
        []  -> do 
            putStrLn "Használat előtt fordítsa le a használni kívánt dll modult!"
            ns <- prompt "Adja meg a C# névtér nevét: "
            classname <- prompt "Adja meg a generálandó C# osztály nevét: "
            path' <- prompt "Adja meg a fordítás után kapott C/C++ header (<haskell fájlnév>_stub.h)\nfájl útvonalát: "
            putStr' "Adja meg a fordítás után kapott .dll könyvtár útvonalát!\n"
            dllpath' <- getLine
            putStrLn "\nA dinamikus könyvtár létezésének ellenőrzése!\nEzen lehetőséggel tetszőleges útvonalat elfogad és a\nfelhasználóra van bízva a könyvtár helyes használata."
            putStr' "Helyes útvonal ellenőrzése [i / n]: "
            ans <- getChar
            whileM_ getChar (hReady stdin)
            let ignore = ans `notElem` "iI"
            dllpath <- (if ignore then return else canonicalizePath) dllpath'
            path <- (if ignore then return else canonicalizePath) path'
            generateCSFile ns classname path dllpath ignore
        [ns,cn,hpath,dllpath] -> do
            hpath' <- canonicalizePath hpath
            dllpath' <- canonicalizePath dllpath
            generateCSFile ns cn hpath' dllpath' False
        [ns,cn,hpath,dllpath, "--ignore-dll"] -> generateCSFile ns cn hpath dllpath True
        ["--test"] -> void runTests
        _   -> do { putStrLn "Nem megfelelő az argumentumok száma vagy formátuma!\nA program kilép!"; threadDelay 1000000}
    where
        generateCSFile :: String -> String -> FilePath -> FilePath -> Bool -> IO ()
        generateCSFile ns cn hpath dllpath ignoreDllPath = do
            errorOrDone $ checkAndGenerate ns cn hpath dllpath ignoreDllPath
            putStrLn "A program kilép!"
            threadDelay 1000000

checkAndGenerate :: String -> String -> FilePath -> FilePath -> Bool -> IO (Either String ())
checkAndGenerate ns cl hpath dllpath ignoreDllPath = do
    h <- doesFileExist hpath
    if h then do
        dll <- doesFileExist dllpath
        if dll || ignoreDllPath then generateCode ns cl hpath dllpath >>= 
            \case Left msg -> return $ Left msg; Right namespace -> return <$> generateCodeFile namespace
        else return $ Left "A megadott útvonalon a .dll fájlt nem lehet megtalálni!"
    else return $ Left "A megadott útvonalon a .h fájlt nem lehet megtalálni!"

errorOrDone :: IO (Either String ()) -> IO ()
errorOrDone e = do
    err <- e
    case err of
        Left str -> putStrLn str
        _        -> putStrLn "A művelet kész!\nA generált fájl megtalálható a futtatási hellyel azonos mappában!"